#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
	float x = 24.0;
	cout << x << endl;
	cout.setf(ios::showpoint); //Show decimal point
	cout << x << endl;
	cout.setf(ios::showpos); //??? - shows positive or negative
	cout << x << endl;
	cout.setf(ios::scientific); //Force scientific notation
	cout << x << endl;
	cout.setf(ios::uppercase); //No effect on number, but force all letters to print in uppercase
	cout << x << endl;
	cout.unsetf(ios::showpoint); //Show decimal point
	cout << x << endl;
	cout.unsetf(ios::showpos); //??? - shows positive or negative
	cout << x << endl;
	cout.unsetf(ios::uppercase); //Force scientific notation
	cout << x << endl;
	cout.unsetf(ios::scientific); //No effect on number, but force all letters to print in uppercase
	cout << x << endl;
	//Part 2
	cout << "PART 2" << endl;
	x = 23.432;
	cout << x << endl;
	cout.setf(ios::fixed);
	cout << setprecision(1) << x << endl; //Set precision to 1 decimal place - prints only 1 digit
	cout << "PART 3" << endl;
	cout << setw(10) << x;
	cout << setw(10) << x;
	cout << setw(10) << x;
	cout << setw(10) << x;
	return 0;
}
