QT += core
QT -= gui

TARGET = school
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp

DISTFILES += \
    README.md

QMAKE_CXXFLAGS += -std=c++1y
